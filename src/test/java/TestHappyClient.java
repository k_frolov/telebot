/**
 * Created by kirill on 11.06.16.
 */

import org.junit.Test;
import static junit.framework.Assert.*;

public class TestHappyClient {

    @Test
    public void testHappy() throws Exception{

        NewHappyClient testHappy = new NewHappyClient();

        assertEquals(30, testHappy.bonus("68 69"));
        assertEquals(30, testHappy.bonus("67 69"));
        assertEquals(20, testHappy.bonus("66 69"));

    }
}
