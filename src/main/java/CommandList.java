
/**
 * Created by kirill on 09.06.16.
 * <p>
 * Обработка команд
 */
class CommandList {

    private static final String HELP = "/help";
    private static final String START = "/start";
    private static final String HAPPY = "Счастье Клиента";
    private static final String KNOW = "Мастер КТЗ";

    String commandDoIt(String comm) {

        switch (comm) {

            case HELP:
                return "Привет, я робот! Я помогу посчитать тебе твою премию. \nНабери /start для начала!";

            case START:
                return "Итак, пока я могу посчитать только две премии: \n /happy - твое \"Счастье Клиента\"; \n /know - твою премию \"Мастер КТЗ\".";

            case "Начало":
                return "Итак, пока я могу посчитать только две премии: \n /happy - твое \"Счастье Клиента\"; \n /know - твою премию \"Мастер КТЗ\".";

            case HAPPY:
                return "Введи количество полученых тобой пятерок и общее количество оценок через пробел. \nНапример: 73 75";

            case KNOW:
                return "Введи общий процент теста и минимальный блок через пробел. \nНапример: 95 85";

            default:
                return null;

        }
    }
}
