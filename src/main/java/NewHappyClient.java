/**
 * Created by kirill on 09.06.16.
 * <p>
 * Другой расчет премии СК
 * <p>
 * Возваращает не знаечение, а ответ целиком
 */

class NewHappyClient {

    private static final int EXEP_NULL = -100;
    private static final int EXEP_ORDER = -101;
    private static final int EXEP_WHAT = -102;

    String getAnswer(String msg) {

        switch (bonus(msg)) {

            case EXEP_NULL:
                return "Ошибка - количество оценок не может быть отрицательным. \nПопробуй еще раз: ";

            case EXEP_ORDER:
                return "Ошибка - не верный порядок значений. \nУкажи сначала количество пятерок, " +
                        "а затем через пробел общее" +
                        " количество оценок. \nПопробуй еще раз:";

            case EXEP_WHAT:
                return "Что-то пошло не так!";
            
            default:
                return "Твоя премия \"Счастье Клиента\": " + bonus(msg) + '%';
        }
    }

    int bonus(String msg2) {

        String[] parts = msg2.split(" ");
        float five = Float.parseFloat(parts[0]);
        float ocenki = Float.parseFloat(parts[1]);
        float happy = (five / ocenki) * 100;

        if ((ocenki < 0) || (five < 0)) {
            return -100; //ошибка - отрицательное количество;
        } else {
            return getPremia(happy, ocenki);
        }
    }

    private int getPremia(float hap, float all) {

        if (hap < 90) {
            return 0;
        }
        if ((hap >= 90) && (hap < 93)) {
            return 10;
        }
        if ((hap >= 93) && (hap < 96)) {
            if (all >= 70) {
                return 30;
            }
            if ((all >= 50) && (all < 70)) {
                return 20;
            }
            if ((all >= 20) && (all < 50)) {
                return 15;
            }
        }
        if ((hap >= 96) && (hap <= 100)) {
            if (all >= 70) {
                return 50;
            }
            if ((all >= 50) && (all < 70)) {
                return 30;
            }
            if ((all >= 20) && (all < 50)) {
                return 20;
            }
        }
        if (hap > 100) {
            return -101; //неверный порядок, счастье больше 100
        } else {
            return -102; // неизвествная ошибка
        }
    }
}

