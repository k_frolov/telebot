import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import java.util.ArrayList;
import java.util.List;

public class SimpleBot extends TelegramLongPollingBot {

    private static final String HAPPY = "Счастье Клиента";
    private static final String KNOW = "Мастер КТЗ";
    private static final String HELP = "/help";
    private static final String START = "Начало";

    private String menu = ""; //выбраный пункт меню

    public static void main(String[] args) {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new SimpleBot());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return "mfr_szf_bot";
    }

    @Override
    public String getBotToken() {
        return "233061433:AAFFFeMjFMFjqCVsiy7zHJbLiMU9C6Gn8xs";
    }


    @Override
    public void onUpdateReceived(Update update) {

        Message message = update.getMessage();

        if (message != null && message.hasText()) {

            CommandList cmd = new CommandList();

            if (message.getText().equals(HAPPY)
                    || message.getText().equals("Начало")
                    || message.getText().equals(KNOW)
                    || message.getText().equals(HELP)
                    || message.getText().equals(START)) {
                this.menu = message.getText();
            }

            if (cmd.commandDoIt(message.getText()) != null) {
                sendMsg(message, cmd.commandDoIt(message.getText()));
            }

            if (this.menu.equals(HAPPY)) {
                NewHappyClient happy = new NewHappyClient();
                sendMsg(message, happy.getAnswer(message.getText()));
            } else if (this.menu.equals(KNOW)) {
                NewKTZ knowledge = new NewKTZ();
                sendMsg(message, knowledge.getAnswer(message.getText()));
            } else if (this.menu.isEmpty()) {
                sendMsg(message, "Введи команду или /help для справки.");
            }

        } else
            sendMsg(message, "Я не воспринимаю такой формат!");
    }

    private void sendMsg(Message message, String text) {


        List<KeyboardRow> commands = new ArrayList<>();

        KeyboardRow commandRow = new KeyboardRow();
        commandRow.add(START);
        commands.add(commandRow);

        KeyboardRow commandRow2 = new KeyboardRow();
        commandRow2.add(HAPPY);
        commands.add(commandRow2);

        KeyboardRow commandRow3 = new KeyboardRow();
        commandRow3.add(KNOW);
        commands.add(commandRow3);

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setKeyboard(commands);

        SendMessage sendMessage = new SendMessage();
        sendMessage.setReplayMarkup(replyKeyboardMarkup);
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setText(text);
        try {
            sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}