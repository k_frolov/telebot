/**
 * Created by kirill on 12.06.16.
 */

class NewKTZ {

    private static final int EXEP_NULL = -100;
    private static final int EXEP_WHAT = -102;
    private static final int EXEP_BIG = -103;

    String getAnswer(String msg) {

        switch (getBonus(msg)) {
            case EXEP_NULL:
                return "Ошибка - результат теста не может быть отрицательным!\n" +
                        "Попробуй еще раз:";
            case EXEP_WHAT:
                return "Что-то пошло не так!";
            case EXEP_BIG:
                return "Ошибка - результат теста не может быть более 100 %!\n" +
                        "Попробуй еще раз:";
            default:
                return "Твоя премия \"Мастер КТЗ\": " + getBonus(msg) + "%";
        }
    }

    int getBonus(String msg2) {

        String[] parts = msg2.split(" ");
        int full = Integer.parseInt(parts[0]);
        int block = Integer.parseInt(parts[1]);

        if ((full < 0) || (block < 0)) {
            return -100; //отрицательные значения
        } else if ((full > 100) || (block > 100)) {
            return -103; //значения больше 100%
        } else {
            return getPremia(full, block);
        }

    }

    private int getPremia(int all, int min) {

        if (all < 92 || min < 80) {
            return 0;
        } else if ((all >= 92 && all < 95) || (min >= 80 && min < 87)) {
            return 10;
        } else if ((all >= 95 && all < 98) || (min >= 87 && min < 93)) {
            return 20;
        } else if (all >= 98 && min >= 93) {
            return 30;
        } else {
            return -102; //Неизвестная ошибка
        }
    }
}
